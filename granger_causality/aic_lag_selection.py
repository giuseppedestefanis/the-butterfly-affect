#
# some trials on akaike information criterion for lag values selection
#

import numpy as np
import statsmodels.api as sm
from saver_decorator import Saver
from granger_causality_test import GrangerTest


class AIC():
 
    
    def __init__(self, metric):
        self.metric = metric
   
     
    def granger_vector_method(self):
        # creating data
        g_instance = GrangerTest(self.metric)
        # first column bitcoin close price series, second column metric series
        g_vector = g_instance.granger_vector(reverse=True)
        
        return g_vector


    def X_y_generator(self, data, lag):
        
        X = []
        y = []
        for i in range(0, len(data)-lag):
            x_metric = data[i:i+lag, 1]
            x_btc = data[i:i+lag, 0]
            X.append(list(x_btc) + list(x_metric))
            y.append(data[i+lag, 0])
            
        return np.array(X), np.array(y)
    
#    @Saver('akaike_results/eth_akaike_results', 'dominance.pkl')
    def compute_akaike(self, data, maxlag):
        
        aic = []
        bic = []
        ssr = []
        # p must start from 1 up to maxlag
        for p in range(1, maxlag+1):
            X, y = self.X_y_generator(data, p)
            model = sm.OLS(y, sm.add_constant(X))
            results = model.fit()
            aic.append(results.aic)
            bic.append(results.bic)
            ssr.append(results.ssr)
        min_aic = [(i+1, min(aic)) for i in range(len(aic)) if aic[i] == min(aic)]
        
        return {'aic': aic, 'min_aic': min_aic[0], 'bic': bic, 'ssr': ssr}

 
if __name__ == '__main__':
    aic_inst = AIC('dominance')
    g_data = aic_inst.granger_vector_method()
    res_akaike = aic_inst.compute_akaike(g_data, maxlag=150)        
        