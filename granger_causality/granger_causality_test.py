#
# N. Uras - 11/07/2019
# Granger causality test.
#


import os
import pandas as pd
from saver_decorator import Saver
this_dir, _ = os.path.split(__file__)
data_path = os.path.join(this_dir, 'stationary_data')
from statsmodels.tsa.stattools import grangercausalitytests


class GrangerTest():
    
    
    def __init__(self, metric):
        self.metric = metric
        self.data = pd.read_csv(os.path.join(data_path, 'stationary_merge_data.csv'))
     
        
    def granger_vector(self, reverse=False):
        # bidimensional array: first column for y, second column for predictor
        if reverse:
            g_vector = self.data.loc[:, [self.metric, 'btc_close']].values
        else:
            g_vector = self.data.loc[:, ['btc_close', self.metric]].values
        
        return g_vector
        
    
#    @Saver('reverse_granger_results/BTC', 'love_pvalues.pkl')    
    def compute_granger_test(self, max_lag, reverse_causalty):
        
        g_vector = self.granger_vector(reverse=reverse_causalty)
        res = grangercausalitytests(g_vector, maxlag=max_lag)
        pvalues = [res[i][0]['params_ftest'][1] for i in range(1, max_lag+1)]
        min_pvalue = [(i+1, j) for i, j in enumerate(pvalues) if j == min(pvalues)]
        
        return pvalues, min_pvalue, res
    
    
if __name__ == '__main__':
    
    g_test = GrangerTest('sadness')
    lag_range = 150
    pvalues, min_pv, res = g_test.compute_granger_test(max_lag=lag_range, reverse_causalty=False)
