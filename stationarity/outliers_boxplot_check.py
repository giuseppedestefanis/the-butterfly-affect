#
# N. Uras - 22/07/2019
# Script to check outliers presence in metrics and bitcoin series
#

import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

data = pd.read_csv('eth_merge.csv')

# metrics
sns.boxplot(data=data.iloc[:, 2:])
#plt.xticks(rotation=20)
plt.ylabel('Values')
plt.title('Metric Distributions')

# bitcoin close price series
data.rename(columns={'btc_close':'Bitcoin close price series'}, inplace=True)
sns.boxplot(x='Bitcoin close price series', y='', data=data)
data[''] = ['BTC' for i in range(len(data))]
plt.title('Boxplot - Bitcoin close prices distribution')

# Per boxplot: se voglio specificare x e y devo trovare in data i nomi che specifico
# in x e y. Ad esempio voglio plottare i prezzi in base ai giorni della settimana.
# Allora in data ci sarà una colonna denominata prezzi con tutti i valori dei prezzi, 
# e una colonna denominata days, lunga quanto prezzi ovviamente, che contiene 
# tutti i giorni. Ovviamente ciascun prezzo deve essere associato al giorno corretto, e 
# viceversa.
