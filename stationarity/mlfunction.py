#
# N. Uras - 28/05/2019
# In this file we implement the MlFunction class that aims to invoke the 
# Preprocessing and Stationarity class in order to get a final output 
# cointaining the results of the considered time series statistical test.
#

import os
import joblib
from importlib import import_module


class MlFunctionClass():
    
    
    def __init__(self, relative_path, features):
        self.relative_path = relative_path
        self.features = features
        
    
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            if func.__name__ == 'from_pd_to_csv':
                path = self.relative_path.replace('data/bitcoin', 'stationarity/clean_data.csv')
                if not os.path.exists(path):
                    func(self, *args, **kwargs)
                else:
                    print('\n')
                    print('clean_data.csv file already exist\n')
            else:
                path = self.relative_path.replace(
                        'data/bitcoin', 'stationarity/results_{}'.format(kwargs['regr']))
                if args[1]+'.pkl' not in os.listdir(path):
                    func(self, *args, **kwargs)
                else:
                    print('\n')
                    print('{} feature already exist\n'.format(args[1]))
                
        return wrapper
    
    
    @decorator                
    def from_pd_to_csv(self, df_to_save):
        df_to_save.to_csv(self.relative_path.replace(
                'data/bitcoin', 'stationarity/clean_data.csv'), index=False)
    
    @decorator
    def saver_results(self, results, feature, regr=None):
        joblib.dump(results, os.path.join(self.relative_path.replace(
                        'data/bitcoin', 'stationarity/results_{}'.format(regr)), '{}.pkl'.format(feature)))
        
        
    def compute_mlfunction(self, freq, regr):
        # creating Preprocessing class instance
        prep = getattr(import_module('Preprocessing'), 'PreprocessingClass')(self.relative_path)
        # merging df
        merged_df = prep.data_merger()
        # encoding categorical variable 
        merged_df = prep.encoder(merged_df, 'politeness')
        # converting dataset to daily frequency
        daily_aggregated_df = prep.time_aggregator(merged_df,
                                                   freq=freq)
        # saving daily_aggregated_df as csv file
        self.from_pd_to_csv(daily_aggregated_df)
        for feature in self.features:
            if feature in daily_aggregated_df.columns:
                # creating Stationarity class instance
                stat = getattr(import_module('stationarity_check'), 'Stationarity')(
                        daily_aggregated_df[feature])
                # calling compute_stationarity check method
                result = stat.compute_stationarity_check(regr)
                # saving results
                self.saver_results(result, feature, regr=regr)
            else:
                raise KeyError('{} not found, missing feature'.format(feature))


if __name__ == '__main__':
    
    # creating MlFunction class instance
    relative_path = '/Users/nicolauras/Documents/MLprojects/blockchain_social_mining/data/bitcoin'
    features = ['sentiment', 'arousal', 'valence', 'dominance', 
                'anger', 'sadness', 'joy', 'love', 'politeness']
    mlf = MlFunctionClass(relative_path, features)
    # calling compute_mlfunction method
    mlf.compute_mlfunction(freq='d', regr='c')
    