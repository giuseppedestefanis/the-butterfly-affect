#
# N. Uras - 12/06/2019
# This file aims to perform a log transformation, in order to stabilize the 
# non-constant variance of a series, after a good analysis of the time series
# plot. So this file is useful only for time series that are non-stationary
# because their non-constant variance over time.
# Once the log transformation is performed, the series is made stationary by 
# differencing, so probably dealing with series that have stochastic trend.
#

import numpy as np
import pandas as pd
from stationarity_check import Stationarity


class StationaryMaker():
    
    
    def __init__(self, cleaned_data_path, feature):
        data = pd.read_csv(cleaned_data_path, sep=';|,', engine='python')
        self.time_series = data[feature].values
        
        
    def log_transform(self):
        # performing log transformation, if zeros are present in time series, 
        # this will lead to a zero division that must be avoided
        log_series = np.log(self.time_series)
        # removing infinities from log_arousal
        for i, item in enumerate(log_series):
            if np.isinf(item):
                log_series[i] = 0
                
        return log_series
    
    
    def difference(self, log_series):
        
        diff_series = [log_series[i] - log_series[i-1] for i in range(1, len(log_series))]
        
        return diff_series
    
    
    def d_differencing(self, log_series, D):
        
        res = []
        for i in range(0, D):
            d_series = self.difference(log_series)
            res.append(d_series)
            log_series = d_series
            
        return res
    
    
    def compute_stationarity_test(self, d_series, regr):
        # creating Stationarity class instance
        stat = Stationarity(d_series)
        results = stat.compute_stationarity_check(regr)
        
        return results
    

if __name__ == '__main__':
    # creating StationaryMaker instance
    data = '/Users/nicolauras/Documents/MLprojects/blockchain_social_mining/stationarity/eth_merge.csv'
    maker = StationaryMaker(data, 'eth_close')
    log_arousal = maker.log_transform()
    all_d_series = maker.d_differencing(log_arousal, D=4)
    test_results = maker.compute_stationarity_test(all_d_series[0], regr='c')
