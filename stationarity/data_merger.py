#
# N. Uras - 19/97/2019
# In this file we merge the bitcoin close price series dataset with that of 
# affect metrics, filtering on date. This filter operation is useful because 
# we want to test for Granger correlation between btc close series and each 
# affect metric series.
#

import os
import pandas as pd
this_dir, _ = os.path.split(__file__)


class DataMerger():
    
    
    def __init__(self):
        self.metrics = pd.read_csv(os.path.join(this_dir, 'clean_data.csv'))
        self.btc = pd.read_csv(os.path.join(this_dir, 'BTC.csv'))
        self.btc.rename(columns={'Date':'created_at'}, inplace=True)
        
    
    def btc_data_cleaner(self, series):

        check_uniqueness = []
        indexes_to_remove = []
        unique = []
        for i, j in enumerate(series['created_at']):
            if j not in check_uniqueness:
                unique.append(j)
                check_uniqueness.append(j)
            else:
                indexes_to_remove.append(i)
        
        series.drop(index=indexes_to_remove, axis=0, inplace=True)
        series.reset_index(inplace=True, drop=True)
    
    
    def data_merger_method(self):
        
        self.btc_data_cleaner(self.btc)
        merge_btc = self.btc.merge(self.metrics, how='inner', on='created_at')
        merge_btc.drop(['Open', 'High', 'Low', 'Volume', 'Adj Close'], axis=1, inplace=True)
        merge_btc.rename(columns={'Close':'btc_close'}, inplace=True)
        
        return merge_btc
    

if __name__ == '__main__':
    dt_instance = DataMerger()
    clean_data = dt_instance.metrics
    btc = dt_instance.btc
    merge_data = dt_instance.data_merger_method()
#    merge_data.to_csv(os.path.join(this_dir, 'data_merge.csv'), index=False)
