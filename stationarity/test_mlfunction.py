#
# N. Uras - 05/07/2019
# Debugging file for mlfunction
#

import os
import joblib
import pandas as pd
from mlfunction import MlFunctionClass

relative_path = '/Users/nicolauras/Documents/MLprojects/blockchain_social_mining/data/bitcoin'
features = ['sentiment', 'arousal', 'valence', 'dominance', 
            'anger', 'sadness', 'joy', 'love', 'politeness']
mlf = MlFunctionClass(relative_path, features)
# calling compute_mlfunction method
mlf.compute_mlfunction(freq='d', regr='ct')

# analysing results
path = '/Users/nicolauras/Documents/MLprojects/blockchain_social_mining/stationarity/results_ct'

res = [(file.replace('.pkl', ''), joblib.load(os.path.join(path, file))) for file in os.listdir(path)]

# =============================================================================
# plotting metrics and bitcoin time series
# =============================================================================
import plotly.offline as py
import plotly.graph_objs as go
from plotly.offline import init_notebook_mode, plot, iplot
py.init_notebook_mode(connected=True)

data = pd.read_csv('eth_merge.csv')


trace1 = go.Scatter(x=data.created_at,
                    y=data['arousal'], 
                    name='Arousal Time Series')
                                                                  
data=go.Data([trace1])
layout=go.Layout(legend=dict(x=0.1, y=0.88), showlegend=True, 
                 xaxis={'title':'Dates', 'showgrid':True}, 
                 yaxis={'title':'Values', 'showgrid':True})
figure=dict(data=data,layout=layout)
plot(figure, filename='arousal_series_plot.html')

# =============================================================================
# btc and eth plot with different y scale
# =============================================================================
import matplotlib.pyplot as plt

from datetime import datetime
dates = []
for ts in data.created_at:
   local_d = datetime.strptime(ts, "%Y-%m-%d").date()
   dates.append(local_d)

x = dates
y1 = data['btc_close']
y2 = data['eth_close']

fig, ax1 = plt.subplots()

ax2 = ax1.twinx()
ax1.plot(x, y1, 'b-')
ax2.plot(x, y2, 'r-')

plt.setp(ax1.xaxis.get_majorticklabels(), rotation=50)
plt.setp(ax2.xaxis.get_majorticklabels(), rotation=50)

ax1.set_xlabel('Date')
ax1.set_ylabel('Bitcoin', color='b')
ax2.set_ylabel('Ethereum', color='r')
plt.title('Bitcoin & Ethereum time series')   

# =============================================================================
# subplot di BTC e ETH con plotly
# =============================================================================
from plotly import tools
import plotly.offline as py
import plotly.graph_objs as go
from plotly.offline import init_notebook_mode, plot, iplot

# opening data
data_btc = pd.read_csv('merge_data.csv')
data_eth = pd.read_csv('eth_merge.csv')

trace1 = go.Scatter(x=data_btc['created_at'],
                    y=data_btc['btc_close'], 
                    mode = 'lines',
                    name='Bitcoin')

trace2 = go.Scatter(x=data_eth['created_at'],
                    y=data_eth['eth_close'], 
                    mode = 'lines',
                    name='Ethereum')

fig = tools.make_subplots(rows=1, cols=2, shared_xaxes=True, shared_yaxes=False)

fig.append_trace(trace1, 1, 1)

fig.append_trace(trace2, 1, 2)

fig['layout'].update()
fig['layout']['xaxis1'].update(title='Date')
fig['layout']['xaxis2'].update(title='Date')
fig['layout']['yaxis1'].update(title='Close prices')

plot(fig, filename='btc_eth_subplot.html')