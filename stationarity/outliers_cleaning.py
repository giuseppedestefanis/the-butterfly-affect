#
# N. Uras - 23/07/2019
# This file aims to remove outliers from data, creating a new csv file
# with data cleaned by outliers.
#


import os
import numpy as np
import pandas as pd
this_dir, _ = os.path.split(__file__)
from stationarity_check import Stationarity


class OutliersCleaner():
    
    
    def __init__(self, metric):
        self.data = pd.read_csv(os.path.join(this_dir, 'eth_merge.csv'))
        self.metric = metric

    def reject_outliers(self, metric, coeff):
        # coeff parameter is the number of standard deviation to consider
        std = np.std(self.data[metric].values)
        data_without_otl = self.data.loc[abs(self.data[metric] - np.mean(
                self.data[metric])) < coeff*std, metric]
        
        return data_without_otl
    
    
    def stationarity_without_outliers(self, coeff, regr):
        data_without_otl = self.reject_outliers(self.metric, coeff)
        stat = Stationarity(data_without_otl)
        res = stat.compute_stationarity_check(regr)
        
        return data_without_otl, res
        

if __name__ == '__main__':
    
    cleaner = OutliersCleaner('love')
    data_cleaned = cleaner.stationarity_without_outliers(2, 'c')
    