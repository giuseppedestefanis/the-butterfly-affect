#
# N. Uras - 27/05/2019
# In this file we implement the Stationarity class that aims to check for time 
# series stationarity through three statistical test: Augmented Dickey-Fuller, 
# Ljung-Box test, KPSS test.
#

from statsmodels.tsa.stattools import kpss
from Preprocessing import PreprocessingClass
from statsmodels.tsa.stattools import adfuller
from statsmodels.stats.diagnostic import acorr_ljungbox


class Stationarity():
    
    
    def __init__(self, time_series):
        self.time_series = time_series
        
    
    def compute_stationarity_check(self, regr):
        
        res_kpss = kpss(self.time_series, regression=regr)
        res_lb = acorr_ljungbox(self.time_series)
        res_adf = adfuller(self.time_series, regression=regr)
        
        summary_dict = {"kpss": res_kpss,
                        "ljung-box": res_lb,
                        "ad-fuller": res_adf}
        
        return summary_dict
        

if __name__ == '__main__':
    
    # creating Preprocessing class instance
    relative_path = '/Users/nicolauras/Documents/MLprojects/blockchain_social_mining/data/bitcoin'
    prep = PreprocessingClass(relative_path)
    # merging df
    merged_df = prep.data_merger()
    # encoding categorical variable 
    merged_df = prep.encoder(merged_df, 'politeness')
    # converting dataset to daily frequency
    daily_aggregated_df = prep.time_aggregator(merged_df,
                                               freq='d')
    # creating Stationarity class instance
    import pandas as pd
    merge_data = pd.read_csv('eth_merge.csv')
    stat = Stationarity(merge_data['eth_close'])
    res = stat.compute_stationarity_check(regr='ct')
