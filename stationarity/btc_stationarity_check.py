#
# N. Uras - 20/06/2019
# This file test for bitcoin close price series stationarity.
#

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from stationarity_check import Stationarity
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

# opening dataset
btc = pd.read_csv('BTC.csv', sep=',')

btc_series = btc['Close'].values

plt.plot(btc_series)

# creating Stationarity class instance
stat = Stationarity(btc_series)

# checking for bitcoin level stationary
level_res = stat.compute_stationarity_check(regr='c')

# checking for bitcon trend stationary
trend_res = stat.compute_stationarity_check(regr='ct')

# =============================================================================
# The Bitcoin close price series turns out to be non stationary with respect to
# both level and trend. We are going to make the series stationary.
# =============================================================================

# let's make the variance constant over time through a logaritmic transform
log_btc = np.log(btc_series)

plt.plot(log_btc)

# taking the first difference of log_btc
log_1_diff = [log_btc[i] - log_btc[i-1] for i in range(1, len(log_btc))]

plt.plot(log_1_diff)

# stationarity check for log_1_diff
stat_2 = Stationarity(log_1_diff)

res_log_diff = stat_2.compute_stationarity_check(regr='c')

# saving stationary bitcoin data
btc.drop(index=0, axis=0, inplace=True)
stat_btc_close = btc.loc[:, ['Date', 'Close']]
stat_btc_close['Close'] = log_1_diff
path = '/Users/nicolauras/Documents/MLprojects/blockchain_social_mining/granger_causality/stationary_data/stationary_btc.csv'
stat_btc_close.to_csv(path, index=False)
# now the bitcoin series turns out to be level stationary and the log_1_diff 
# series is not a unit root
# The bitcoin close price series therefore has a stochastic trend and it is 
# a unit root. It is an integrated series of order 1.

# =============================================================================
# Let's see if the btc series has instead a deterministic trend, so let's try
# to evaluate it by model fitting and then to remove it from the series.
# =============================================================================

# evaluating trend with Polynomial Regression model

X = np.reshape(btc_series, (len(btc_series), 1))
preds = []
for i in range(2, 20):
    poly = PolynomialFeatures(degree=i)
    X_poly = poly.fit_transform(X)
    model = LinearRegression()
    model.fit(X_poly, btc_series)
    # calculate trend
    trend = model.predict(poly.fit_transform(X))
    preds.append({i: trend})
    
plt.plot(btc_series)
plt.plot(preds[3][5])    
