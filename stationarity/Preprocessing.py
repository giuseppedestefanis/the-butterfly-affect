#
# N. Uras - 12/04/2019
# This file is the first step towards the stationarity check of different 
# time series. It deals with time series creation of the considered feature 
# with the desired time frequency.
#

import os
import pandas as pd
from sklearn.preprocessing import OneHotEncoder


class PreprocessingClass():
    """This class aims to handle the data of the issue_comments and 
    issue_comments_text_affect csv file, based on a comment_id key check.
    The only required attribute is the relative path in which
    to find the two csv files mentioned above.
    """
    
    
    def __init__(self, relative_path):
        self.relative_path = relative_path
        
        
    def data_merger(self, key='comment_id'):
        """This method aims to open the csv files and to intersect the two 
        datasets based on the chosen control key.
        
        ******************************************************************
        
        Params:
            key: control key shared by the two datasets (str).
        Returns:
            df_merged: merged dataset (pandas Dataframe).
        """
        
        # opening first csv file
        df_1 = pd.read_csv(os.path.join(self.relative_path, 'issue_comments.csv'), sep=',')
        # opening second csv file
        df_2 = pd.read_csv(os.path.join(
                self.relative_path, 'issue_comments_text_affect.csv'), sep=',')
        # merging datasets on comment_id feature with intersection mode
        df_merged = df_2.merge(df_1, how='inner', on=key)
        # from string to datetime conversion
        df_merged['created_at'] = pd.to_datetime(df_merged.created_at)
        # ordering by increasing date
        df_merged.sort_values(by='created_at', inplace=True)
        # resetting index
        df_merged.reset_index(drop=True, inplace=True)
        
        return df_merged
    
    
    def encoder(self, dataset, feature_to_encoder):
        """This method allows to encode any categorical variables present 
        in the dataset, using the OneHotEncoder provided by sklearn library.
        
        **********************************************************************
        
        Params:
            dataset: pandas dataframe containing the categorical features to 
            encoder;
            feature_to_encoder: categorical feature to be encoded (str).
        Returns:
            df_merged: pandas dataframe with the categorical feature encoded.
        """
        # creating OneHotEncoder instance
        hot_encoder = OneHotEncoder(categories='auto', sparse=False)
        # calling the fit_transform method
        encoded_feature = hot_encoder.fit_transform(dataset[feature_to_encoder].values.reshape(-1, 1))
        # avoiding dummy variables trap
        encoded_feature = encoded_feature[:, 1:]
        # updating dataset with the encoded feature
        dataset[feature_to_encoder] = encoded_feature
        
        return dataset
    
    
    def time_aggregator(self, merged_df, freq='d'):
        """This method aims to create the time structure of the considered 
        series, daily or monthly, grouping all the data related to the same 
        day or month.
        
        *******************************************************************
        
        Params:
            merged_df: dataset containing the considered feature (pandas Dataframe);
            feature: feature to be grouped (str);
            freq: aggregating frequency (str).
        Returns:
            grouped_df: dataframe cointaining both date and feature values 
            informations (pandas Dataframe);
            series_values: array containing only the values, ready to be 
            processed (numpy array).
        """
        # selecting only the useful features
        features_to_kept = [col for col in merged_df.columns if col not in [
                'comment_id', 'issue_id', 'user_id']]
        df = merged_df.loc[:, features_to_kept]
        # aggregating data by date
        grouped_df = df.set_index(
                'created_at').groupby(pd.Grouper(freq=freq)).mean().dropna(how='all')
        # resetting indeces inplace
        grouped_df.reset_index(drop=False, inplace=True)
        if freq == 'd':
            # we are only interested in date
            grouped_df['created_at'] = getattr(grouped_df, 'created_at').dt.date
        elif freq == 'm':
            # in this case we are interested only in year and month
            date_list = [i.split('-') for i in grouped_df['created_at'].astype(str)]
            year_month = ['-'.join(item[:2]) for item in date_list]
            grouped_df['created_at'] = year_month
        else:
            raise TypeError('\'{}\' frequency mode is not available'.format(freq))
        
        return grouped_df
            
       
# testing class
if __name__ == '__main__':
    
    # creating Preprocessing instance
    relative_path = '/Users/nicolauras/Documents/blockchain_social_mining/data/bitcoin'
    prep = PreprocessingClass(relative_path)
    # calling data_merger method
    merged_df = prep.data_merger()
    # calling the encoder method in order to encode the politeness categorical feature
    merged_df = prep.encoder(merged_df, 'politeness')
    # calling time aggregator method with daily frequency
    daily_aggregated_df = prep.time_aggregator(merged_df,
                                               freq='d')
    # calling time aggregator method with monthly frequency
    monthly_aggregated_df = prep.time_aggregator(merged_df, 
                                                 freq='m')
