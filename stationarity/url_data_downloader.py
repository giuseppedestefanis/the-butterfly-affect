
import io
import datetime
import requests
import numpy as np
import pandas as pd

class UrlDataGenerator():
    
    def __init__(self, financial_series, start_date, end_date):
        self.series_name = financial_series
        self.start_date = start_date
        self.end_date = end_date
        self.__frequency = '1d'
        
    
    def timestamp_encoder(self):
        
        # from string to datetime and avoiding shift-day problem
        self.start_date = datetime.datetime.strptime(
                self.start_date, "%d-%m-%Y") + datetime.timedelta(days=1)
        self.end_date = datetime.datetime.strptime(
                self.end_date, "%d-%m-%Y") + datetime.timedelta(days=1)
        # from datetime to timestamp
        self.start_date = int(datetime.datetime.timestamp(self.start_date))
        self.end_date = int(datetime.datetime.timestamp(self.end_date))
        
        
    def data_downloader_as_dataframe(self):
        
        self.timestamp_encoder()
        url = 'https://query1.finance.yahoo.com/v7/finance/download/{}?period1={}&period2={}&interval={}&events=history&crumb=OenjZeH5Shc'.format(self.series_name, self.start_date, self.end_date, self.__frequency)
        r = requests.post(url)
        if r.ok:
            data = r.content.decode('utf8')
            df = pd.read_csv(io.StringIO(data), sep=';|,', engine='python')
            
        return df
    
    
if __name__ == '__main__':
    # enter date in european format: day-month-year
    url_instance = UrlDataGenerator('BTC-USD', '20-12-2010', '31-08-2017')
    btc_dataframe = url_instance.data_downloader_as_dataframe()
    # saving btc series as csv file
    btc_dataframe.to_csv('/Users/nicolauras/Documents/MLprojects/blockchain_social_mining/stationarity/BTC.csv',
                         index=False)
    # opening clean_data
    clean_data = pd.read_csv('clean_data.csv', sep=',')
    # check for missing values in clean_data
    btc_dataframe['presence'] = np.isin(btc_dataframe['Date'], clean_data['created_at'])       
    missing_values = btc_dataframe.loc[btc_dataframe['presence'] == False, 'Date']
    # So the missing values in clean_data are 67.
    