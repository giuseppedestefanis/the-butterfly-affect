import numpy as np
from keras.layers import Dense
from keras.layers import LSTM
from keras.models import Sequential
from matplotlib import pyplot
from pandas import DataFrame
from pandas import concat
from pandas import read_csv
from scipy.stats import ranksums, stats
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler


def supervised_series(data, n_in=1, n_out=1, remove_nan=True):
    variables = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    columns, names = list(), list()
    for i in range(n_in, 0, -1):
        columns.append(df.shift(i))
        names += [('x_%d(t-%d)' % (j + 1, i)) for j in range(variables)]
    for i in range(0, n_out):
        columns.append(df.shift(-i))
        if i == 0:
            names += [('x_%d(t)' % (j + 1)) for j in range(variables)]
        else:
            names += [('x_%d(t+%d)' % (j + 1, i)) for j in range(variables)]
    series = concat(columns, axis=1)
    series.columns = names
    if remove_nan:
        series.dropna(inplace=True)
    return series


def run_experiment_with_affetcs(values, epochs=50, scale=False, n_previous_days=10, training_ration=0.7):
    scaler = MinMaxScaler(feature_range=(0, 1))
    if scale:
        scaled = scaler.fit_transform(values)
        series = supervised_series(scaled, n_previous_days, 1)
    else:
        series = supervised_series(values, n_previous_days, 1)
    n_train = int(len(series.values) * training_ration)
    train = series.values[:n_train, :]
    test = series.values[n_train:, :]
    train_X, train_y = train[:, :-1], train[:, -1]
    test_X, actual = test[:, :-1], test[:, -1]
    train_X = train_X.reshape((train_X.shape[0], 1, train_X.shape[1]))
    test_X = test_X.reshape((test_X.shape[0], 1, test_X.shape[1]))
    model = Sequential()
    model.add(LSTM(50, input_shape=(train_X.shape[1], train_X.shape[2])))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='mae', optimizer='adam', metrics=['mse'])
    history = model.fit(
        train_X,
        train_y,
        epochs=epochs,
        batch_size=72,
        validation_data=(test_X, actual),
        verbose=2,
        shuffle=False
    )
    predicted = model.predict(test_X)
    test_X = test_X.reshape((test_X.shape[0], test_X.shape[2]))
    inv_predicted = np.concatenate((predicted, test_X[:, 1:]), axis=1)
    inv_predicted = inv_predicted[:, 0]
    actual = actual.reshape((len(actual), 1))
    inv_actual = np.concatenate((actual, test_X[:, 1:]), axis=1)
    inv_actual = inv_actual[:, 0]
    rmse = np.sqrt(mean_squared_error(inv_actual, inv_predicted))
    return history, rmse


def run_prediction(epochs, dataset_name, n_features, features):
    models = []
    for n_feature in range(1, n_features + 1):
        dataset = read_csv(
            './data/%s.csv' % dataset_name,
            header=0,
            usecols=features[:n_feature + 1],
            index_col=0,
        )
        dataset.index.name = 'created_at'
        # ensure all data is float
        values = dataset.values.astype('float32')
        models.append(
            run_experiment_with_affetcs(
                values=values,
                epochs=epochs,
                training_ration=0.9,
                scale=True
            )
        )
    for i, model in enumerate(models):
        n_feature = i + 1
        pyplot.plot(model[0].history['loss'], label='Loss %d' % n_feature)
    pyplot.legend()
    pyplot.xlabel("Epochs")
    pyplot.ylabel("Loss")
    fig = pyplot.gcf()
    fig.set_size_inches(5, 5)
    fig.savefig('./imgs/%s_loss.eps' % dataset_name, format='eps', dpi=1200)
    pyplot.show()

    mseData = np.random.random(size=(n_features, epochs))
    for i, model in enumerate(models):
        for j, mse in enumerate(model[0].history['mean_squared_error']):
            mseData[i][j] = mse
    z, p = stats.ranksums(mseData[0], mseData[n_features - 1])
    effect_size = abs(z / np.sqrt(len(mseData[0]) + len(mseData[n_features - 1])))

    with open('./results/%s_results.txt' % dataset_name, 'w')as file:
        file.write("Wilcoxon Test\n")
        file.write("z\tp\teffect_size\n")
        file.write("%.4f\t%.4f\t%.4f\n" % (z, p, effect_size))
        file.write("#_features\tRMSE\n")
        for i, model in enumerate(models):
            n_feature = i + 1
            file.write("%d\t%.3f\n" % (n_feature, model[1]))


if __name__ == '__main__':
    epochs = 50
    run_prediction(
        epochs=epochs,
        dataset_name='merge_data_btc',
        n_features=3,
        features=['created_at', 'btc_close', 'sentiment', 'sadness'],
    )
    run_prediction(
        epochs=epochs,
        dataset_name='merge_data_eth',
        n_features=7,
        features=['created_at', 'eth_close', 'sentiment', 'anger', 'arousal', 'valence', 'dominance', 'love'],
    )
